module.exports = { 
  "extends": "airbnb-base",
  "rules": {
    "no-plusplus" : 'off',
    "no-param-reassign" : 'off',
    "array-callback-return" : 'off',
  }
};