// const fs = require('fs');
const aws = require('aws-sdk');
const config = require('config');

const params = {
  Bucket: 'ambee-certificate',
  Key: 'ca-cert',
};

const s3 = new aws.S3({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
});

aws.config.update({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
});

function getClientOptions() {
  return new Promise((res, rej) => {
    s3.getObject(params, (err, data) => {
      if (err) rej(err);
      else {
        res({
          kafkaHost: 'kafka.getambee.com:9193,kafka.getambee.com:9194,kafka.getambee.com:9195',
          ssl: true,
          sslOptions: {
            host: 'kafka.getambee.com',
            ca: [data.Body.toString()],
          },
        });
      }
    });
  });
}

module.exports = getClientOptions;
