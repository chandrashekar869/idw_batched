/* eslint-disable class-methods-use-this */
const async = require('async');
const cp = require('child_process');
const _ = require('lodash');
// const fs = require('fs');
const config = require('config');

const s3 = require('./s3');

let startTime;
class BatchRequests {
  generateQueue() {
    this.queue = async.queue((object, callback) => {
      // let i = object.zipcodes.length;
      startTime = new Date().getTime();
      this.generateKeyId(object.stationData, () => {
        this.generateParamBasedStations(object.stationData, object.paramsToInterpolate)
          .then((data) => {
            async.map(object.zipcodes, (chunk, chunksCb) => {
            // i--;
              const child = cp.fork('./interpolate.js');
              child.send({
                chunk,
                data,
                childCount: this.childCount[object.requestCount]++,
                countryCode: object.countryCode,
                stationData: object.stationData,
                paramsToInterpolate: object.paramsToInterpolate,
                chunkId: object.chunkId++,
              });
              child.addListener('close', () => {
                console.log(new Date().getTime() - startTime);
                console.log('Child exited', this.childCount[object.requestCount]--);
                if (this.childCount[object.requestCount] === 0) {
                  console.log('Done', object.requestCount);
                  callback();
                }
              });
              chunksCb();
            }, (chunksErr) => {
              if (chunksErr) throw chunksErr;
              else console.log('Done');
            });
          })
          .catch((err) => {
            console.log(err);
          });
      });
    }, this.concurrency);

    this.queue.drain = () => {
      if (this.requestCount > 10) {
        this.requestCount = 0;
        this.childCount = {};
      }
    };
  }

  constructor(concurrency) {
    this.requestCount = 0;
    this.childCount = {};
    this.concurrency = concurrency;
    this.generateQueue();
    console.log('Queue initialised');
  }

  generateKeyId(stations, callback) {
    stations.map((val, index) => {
      val.keyId = index;
      if (index === stations.length - 1) callback();
    });
  }

  generateParamBasedStations(stations, params) {
    return new Promise((res, rej) => {
      const paramBasedStations = {};
      async.map(params, (param, cb) => {
        const paramBasedStation = stations.filter(station => station[param] !== undefined);
        if (paramBasedStation.length > 0) paramBasedStations[param] = paramBasedStation;
        cb();
      }, (err) => {
        if (err) rej(err);
        else {
          res(paramBasedStations);
        }
      });
    });
  }

  async addToQueue(object) {
    this.chunkId = 0;
    this.zipcodes = _.chunk(await JSON.parse(await s3.getFile(config.zipcodeBucketName, `${object.countryCode}.json`)), 5000);
    // console.log(this.zipcodes.length);
    this.childCount[++this.requestCount] = 0;
    console.log(this.zipcodes.length);
    console.log(this.childCount);
    if (this.queue !== undefined) {
      this.queue.push({
        ...object,
        chunkId: this.chunkId,
        zipcodes: this.zipcodes,
        requestCount: this.requestCount,
      });
    } else throw new Error('Queue doesnt exist');
  }
}

module.exports = new BatchRequests(1);
