const async = require('async');
const cp = require('child_process');
const _ = require('lodash');
// const fs = require('fs');
const config = require('config');

const s3 = require('./s3');

let startTime;

class BatchRequests {
  generateQueue() {
    this.queue = async.queue((object, callback) => {
      // let i = object.zipcodes.length;
      startTime = new Date().getTime();
      async.map(object.zipcodes, (chunk, chunksCb) => {
        // i--;
        const child = cp.fork('./interpolate2.js');
        child.send({
          chunk,
          childCount: this.childCount[object.requestCount]++,
          countryCode: object.countryCode,
          stationData: object.stationData,
          paramsToInterpolate: object.paramsToInterpolate,
          chunkId: object.chunkId++,
        });
        child.addListener('close', () => {
          console.log(new Date().getTime() - startTime);
          console.log('Child exited', this.childCount[object.requestCount]--);
          if (this.childCount[object.requestCount] === 0) {
            console.log('Done', object.requestCount);
            callback();
          }
        });
        chunksCb();
      }, (chunksErr) => {
        if (chunksErr) throw chunksErr;
        else console.log('Done');
      });
    }, this.concurrency);

    this.queue.drain = () => {
      if (this.requestCount > 10) {
        this.requestCount = 0;
        this.childCount = {};
      }
    };
  }
  
  getChildCount() {
    return this.childCount;
  }

  constructor(concurrency) {
    this.requestCount = 0;
    this.childCount = {};
    this.concurrency = concurrency;
    this.generateQueue();
    console.log('Queue initialised');
  }

  async addToQueue(object) {
    this.chunkId = 0;
    this.zipcodes = _.chunk(await JSON.parse(await s3.getFile(config.zipcodeBucketName, `${object.countryCode}.json`)), 5000);
    // console.log(this.zipcodes.length);
    this.childCount[++this.requestCount] = 0;
    console.log(this.zipcodes.length);
    console.log(this.childCount);
    if (this.queue !== undefined) {
      this.queue.push({
        ...object,
        chunkId: this.chunkId,
        zipcodes: this.zipcodes,
        requestCount: this.requestCount,
      });
    } else throw new Error('Queue doesnt exist');
  }
}

module.exports = new BatchRequests(2);
