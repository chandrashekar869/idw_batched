const _ = require('lodash');
const async = require('async');
const geolib = require('geolib');
// const fs = require('fs');
const helpers = require('./helpers');

let i = 0;
let chunkId;
let rId;
let lastUpdate;

function genRandom(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

// function generateKeyId(stations, callback) {
//   stations.map((val, index) => {
//     val.keyId = index;
//     if (index === stations.length - 1) callback();
//   });
// }

// function generateParamBasedStations(stations, params) {
//   return new Promise((res, rej) => {
//     const paramBasedStations = {};
//     async.map(params, (param, cb) => {
//       const paramBasedStation = stations.filter(station => station[param] !== undefined);
//       if (paramBasedStation.length > 0) paramBasedStations[param] = paramBasedStation;
//       cb();
//     }, (err) => {
//       if (err) rej(err);
//       else {
//         res(paramBasedStations);
//       }
//     });
//   });
// }

function nearestStation(referencePoint, stationList, offset) {
  return geolib.findNearest(referencePoint, stationList, offset);
}

function interpolate(referencePoint, nearestStations, paramsToInterpolate, power, callback) {
  async.map(paramsToInterpolate, (param, paramCb) => {
    let upperSummation = 0;
    let lowerSummation = 0;
    async.map(nearestStations, (station, stationCb) => {
      const value = station.data[param];
      if (value !== undefined && value.value > 0) {
        upperSummation += (Number(station.data[param].value) / (station.distance ** power));
        lowerSummation += (1 / (station.distance ** power));
      }
      stationCb();
    }, (stationErr) => {
      if (stationErr) throw stationErr;
      else {
        const result = upperSummation / lowerSummation;
        if (!Number.isNaN(result)) {
          helpers.genNestedObject(referencePoint, [param]);
          referencePoint[param] = result;
        }
        paramCb();
      }
    });
  }, (paramErr) => {
    if (paramErr) throw paramErr;
    else {
      referencePoint.geoLoc = { type: 'Point', coordinates: [Number(referencePoint.longitude), Number(referencePoint.latitude)] };
      referencePoint.rId = rId;
      referencePoint.lastUpdate = lastUpdate;
      console.log(`${rId}_${i++}`);
      helpers.calculateAQI(referencePoint, (data) => {
        // console.log(data);
        if (data.AQI !== undefined) {
          data.AQI = data.AQI % 2 === 0 ? data.AQI + genRandom(1, 5) : data.AQI - genRandom(1, 5);
        }
        callback();
      });
    }
  });
}

// eslint-disable-next-line max-len
function getNearestStations(referencePoint, paramBasedStations, paramsToInterpolate, offset, callback) {
  const nearestStations = [];
  async.map(paramsToInterpolate, (param, cb) => {
    let index = offset;
    while (index--) {
      const result = nearestStation(referencePoint, paramBasedStations[param], index);
      const station = _.extend(result, {
        data: paramBasedStations[param][result.key],
      });
      // eslint-disable-next-line max-len
      if (nearestStations.findIndex(data => data.data.keyId === station.data.keyId) === -1) nearestStations.push(station);
      if (index === 0) {
        cb();
      }
    }
  }, (err) => {
    if (err) {
      callback();
      throw err;
    } else {
      interpolate(referencePoint, nearestStations, paramsToInterpolate, 2, callback);
    }
  });
}

process.on('message', (message) => {
  // eslint-disable-next-line prefer-destructuring
  chunkId = message.chunkId;
  rId = `${message.countryCode}_${chunkId}`;
  console.log('process called', rId);
  lastUpdate = helpers.geUTCDate();
  async.map(message.chunk, (code, chunkCb) => {
    getNearestStations(code, message.data, message.paramsToInterpolate, 1, chunkCb);
  }, (chunkErr) => {
    if (chunkErr) throw chunkErr;
    else {
      console.log('Done');
      helpers.callRequest({
        rId,
        data: message.chunk,
      }, rId, helpers.getConfigParams('interpolate'), process.exit);
    //  fs.writeFile(`./results/${chunkId}.json`, JSON.stringify(message.chunk), process.exit);
    }
  });
});
process.on('uncaughtException', (err) => {
  console.error(err, 'Uncaught Exception thrown');
  process.exit(1);
});
