const express = require('express');
const bodyParser = require('body-parser');

// const controller = require('./createChild');
const batch = require('./batch');


const app = express();
const router = express.Router();
const port = 80;

app.use((req, res, next) => {
// Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish  to allow
  res.setHeader('Access-Control-Allow-Headers', 'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', false); // Fix this
  // Pass to next layer of middleware
  next();
});

app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json' }));


app.use('/', router);

router.route('/')
  .get((req, res) => {
    const childCount = batch.getChildCount();
    let activeCount = 0;
    Object.keys(childCount).map((key) => {
      if (childCount[key] > 0) activeCount += 1;
    });
    if (activeCount < 4) res.status(200).send(batch.getChildCount());
    else res.status(500).send(batch.getChildCount());
  });


router.route('/interpolate')
  .post((req, res) => {
  // eslint-disable-next-line prefer-destructuring
    const body = req.body;
    res.send(200);
    console.log('called');
    batch.addToQueue({
      stationData: body.data,
      paramsToInterpolate: body.params,
      countryCode: body.country,
    });
    // controller.init(body.data, body.params, body.country);
    // cp.spawn('./child').send(body);
    // queue.push({ body, function: controller.interpolate }, () => {console.log("Done")});
  });

app.listen(port);
