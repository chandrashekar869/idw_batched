const config = require('config');
// const request = require('request');
// const fs = require('fs');
const moment = require('moment');
const kafka = require('@chandu/node-kafka-wrapper');
const aqibot = require('./aqi-bot/index');
const clientOptions = require('./client-options');
const producerOptions = require('./producer-options');

function getConfigParams(param) {
  return config[param];
}

async function callRequest(data, rId, options, callback) {
  kafka.producer.sendMessage(await clientOptions(), producerOptions, [JSON.stringify(data)], 'test11', () => {
    console.log('Sent message to kafka');
    callback();
  });
  // request({
  //   url: options.url,
  //   body: JSON.stringify(data),
  //   method: options.method,
  //   headers: options.headers,
  // }, (err, res) => {
  //   if (err) {
  //     throw err;
  //   } else {
  //     fs.writeFileSync(`./outputLogs/${rId}.json`, JSON.stringify(res));
  //     if (res.statusCode === 502) {
  //       fs.writeFileSync(`./outputLogs/${rId}_errData.json`, JSON.stringify(data));
  //     }
  //     callback();
  //   }
  // });
}

function geUTCDate() {
  return moment().utc().toLocaleString();
}

function genNestedObject(object, names) {
  let base = object;
  for (let i = 0; i < names.length; i += 1) {
    // eslint-disable-next-line no-multi-assign
    base = base[names[i]] = base[names[i]] || {};
  }
}

function calculateAQI(data, cb) {
  const aqiParam = getConfigParams('aqiParams');
  const aqiArray = Object.keys(aqiParam);
  let max = { aqi: 0 };
  // eslint-disable-next-line array-callback-return
  aqiArray.map((param, index) => {
    if (typeof (data[param]) !== 'undefined' && Number(data[param]) > 0) {
      const pollutantType = aqibot.PollutantType[aqiParam[param]];
      aqibot.AQICalculator.getAQIResult(pollutantType, Math.round(data[param]))
        .then((result) => {
          data[param].index = result.aqi;
          max = max.aqi < result.aqi ? result : max;
          data.aqiInfo = {
            pollutant: max.pollutant,
            concentration: max.concentration,
            category: max.category,
          };
          genNestedObject(data, ['AQI']);
          data.AQI = max.aqi;
          if (index === aqiArray.length - 1) {
            cb(data);
          }
        }).catch(() => {
          data[param].index = null;
          if (index === aqiArray.length - 1) {
            cb(data);
          }
        });
    } else if (index === aqiArray.length - 1) {
      cb(data);
    }
  });
}

module.exports = {
  geUTCDate,
  callRequest,
  calculateAQI,
  genNestedObject,
  getConfigParams,
};
