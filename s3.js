const aws = require('aws-sdk');
const config = require('config');

const s3 = new aws.S3({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
});

aws.config.update({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
});

const getFile = (bucketName, key) => {
  const params = { Bucket: bucketName, Key: key };
  return new Promise((resolve, reject) => {
    s3.getObject(params, (err, data) => {
      if (err) reject(err);
      else {
        resolve(data.Body.toString());
      }
    });
  });
};


module.exports = { getFile };
