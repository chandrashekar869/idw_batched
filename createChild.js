const fs = require('fs');
const _ = require('lodash');
const async = require('async');
const cp = require('child_process');

//  const stationData = JSON.parse(fs.readFileSync('./genData.json'));
let zipcodes;
//  const paramsToInterpolate = ["AQI","PM25","PM10","NO2","SO2","CO","OZONE"];
//  const startinterpolation = require('./interpolate');

let chunkId;

function forkChild(zipcodeChunk, stationData, paramsToInterpolate, countryCode) {
  async.map(zipcodeChunk, (chunk, chunksCb) => {
    const child = cp.fork('./interpolate.js');
    child.send({
      countryCode,
      chunk,
      stationData,
      paramsToInterpolate,
      chunkId: chunkId++,
    });
    chunksCb();
  }, (chunksErr) => {
    if (chunksErr) throw chunksErr;
    else console.log('Done');
  });
}

function init(stationData, paramsToInterpolate, countryCode) {
  // console.log(fs.readFileSync(`./zipcodes/${countryCode}.json`,'utf-8'));
  console.log(stationData);
  chunkId = 0;
  zipcodes = _.chunk(JSON.parse(fs.readFileSync(`./zipcodes/${countryCode}.json`)), 5000);
  forkChild(zipcodes, stationData, paramsToInterpolate, countryCode);
}

module.exports = { init };
